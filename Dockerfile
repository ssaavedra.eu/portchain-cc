FROM node:latest

RUN npm install -g pnpm

WORKDIR /app

ENV LISTEN_ADDRESS 0.0.0.0

ADD . /app

RUN pnpm install

ENTRYPOINT ["pnpm", "run", "start:norestart"]

