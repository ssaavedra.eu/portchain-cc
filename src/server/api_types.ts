export type Vessel = {
  imo: number
  name: string
}

export type Port = {
  id: string
  name: string
}

export type LogEntry =
  | {
      updatedField: 'arrival'
      arrival: string
      departure?: null
      isOmitted?: null
      createdDate: string
    }
  | {
      updatedField: 'departure'
      arrival?: null
      departure: string
      isOmitted?: null
      createdDate: string
    }
  | {
      updatedField: 'isOmitted'
      arrival?: null
      departure?: null
      isOmitted: boolean
      createdDate: string
    }

export type PortCall = {
  arrival: string
  departure: string
  createdDate: string
  isOmitted: boolean
  service: string
  port?: Port /* This is in API */
  vessel?: Vessel /* We might want to have this one */
  logEntries: LogEntry[]
}

export type VesselInfo = {
  vessel: Vessel
  portCalls: PortCall[]
}

// API Result types

export type GetTopPortsResult = {
  results: {
    portId: string
    totalPortCalls: number
  }[]
  morePortsTied: boolean
  total?: number
}

export type PortCallDurationPercentilesResult = {
  [portId: string]: {
    port: Port,
    portCallDurations: {
      [percentile: string]: number
    }
  }
}

export type PortCallDelayPercentileResult = {
  [vesselIMO: string]: {
    vessel: Vessel,
    portCallDelays: {
      [day: string]: {
        [percentile: string]: number
      }
    }
  }
}
