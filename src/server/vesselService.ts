import type { PortCall, PortCallDelayPercentileResult } from './api_types'
import { getAllVesselInfo } from './api'
import { quantile } from 'simple-statistics'

export class VesselService {
  public async getPortCallDelayPercentiles ({
    percentiles,
    days,
    considerOmitted = false
  }: {
    percentiles: number[]
    days: number[],
    considerOmitted?: boolean,
  }): Promise<PortCallDelayPercentileResult> {
    const allVesselInfo = await getAllVesselInfo()

    const stats = {}

    for (const vesselInfo of allVesselInfo) {
      const allDays = days.map((day) => {
        const delays = vesselInfo.portCalls.map(pc => this.getPortCallDelay(pc, day, considerOmitted))
        const percentilesOutput = Object.fromEntries(
          percentiles.map(percentile => {
            return [percentile, quantile(delays, percentile / 100)]
          })
        )
        return [day, percentilesOutput]
      })

      const pcd = {
        vessel: vesselInfo.vessel,
        portCallDelays: Object.fromEntries(allDays)
      }
      stats[vesselInfo.vessel.imo] = pcd
    }

    return stats
  }

  protected getPortCallDelay (
    pc: Readonly<PortCall>,
    day: number,
    considerOmitted: boolean = false
  ): number {
    const arrived = new Date(pc.arrival)
    const dayMs = day * 24 * 3600 * 1000
    const lastPossibleArrivalTime = arrived.getTime() - dayMs

    const delayItems = pc.logEntries.filter(entry =>
      entry.updatedField === 'arrival'
    ).map(entry =>
      Object.assign({}, entry, { arrivalDate: new Date(entry.arrival), createdDate: new Date(entry.createdDate) })
    ).filter(entry =>
      entry.createdDate.getTime() <= lastPossibleArrivalTime
    ).sort((a, b) =>
      b.createdDate.getTime() - a.createdDate.getTime()
    )

    if (delayItems.length === 0 || (!considerOmitted && pc.isOmitted)) {
      return 0
    } else {
      const delayMs = Math.abs(arrived.getTime() - delayItems[0].arrivalDate.getTime())
      return delayMs / 1000 / 3600
    }
  }
}

export default new VesselService()
