import {
  GetTopPortsResult,
  Port,
  PortCall,
  PortCallDurationPercentilesResult,
  Vessel
} from './api_types'
import { getAllVesselInfo as apiGetAllVesselInfo } from './api'
import { quantile } from 'simple-statistics'

type PortInfo = {
  port: Port
  portCalls: Array<PortCall & { vessel: Vessel }>
}

export class PortService {
  getAllVesselInfo = apiGetAllVesselInfo

  public async getTopPorts ({
    order,
    limit,
    start = 0,
    considerOmitted = false
  }: {
    order: 'asc' | 'desc'
    limit: number
    start?: number
    considerOmitted?: boolean
  }): Promise<GetTopPortsResult> {
    const vesselInfo = await this.getAllVesselInfo()

    const portsByItemMap = new Map<string, number>()
    vesselInfo.forEach(info => {
      info.portCalls.forEach(portCall => {
        const prevMap = portsByItemMap.get(portCall.port.id)
        const prev = prevMap || 0
        if (considerOmitted || !portCall.isOmitted) {
          portsByItemMap.set(portCall.port.id, prev + 1)
        }
      })
    })
    const portsByItem = Array.from(portsByItemMap)

    const comparator =
      order === 'asc'
        ? ([_a, a], [_b, b]) => {
          return a - b
        }
        : ([_a, a], [_b, b]) => {
          return b - a
        }
    const results = portsByItem
      .sort(comparator)
      .slice(start, start + limit + 1)
      .map(port => {
        return {
          portId: port[0],
          totalPortCalls: port[1]
        }
      })
    const morePortsTied =
      limit > 0 &&
      results.length > 1 &&
      results[results.length - 1].totalPortCalls ===
        results[results.length - 2].totalPortCalls

    return {
      morePortsTied,
      total: portsByItem.length,
      results: results.slice(0, limit)
    }
  }

  protected async getAllPortInfo (): Promise<Map<string, PortInfo>> {
    const avi = await this.getAllVesselInfo()
    const ports = new Map<string, PortInfo>()
    avi.forEach(vi => {
      vi.portCalls.forEach(_pc => {
        const pc = Object.assign({}, _pc, { vessel: vi.vessel })
        if (!ports.has(pc.port.id)) {
          ports.set(pc.port.id, {
            port: pc.port,
            portCalls: []
          })
        }
        ports.get(pc.port.id).portCalls.push(pc)
      })
    })
    return ports
  }

  public async getPortCallDurationPercentiles ({
    percentiles,
    considerOmitted = false
  }: {
    percentiles: number[]
    considerOmitted?: boolean
  }): Promise<PortCallDurationPercentilesResult> {
    const allPortInfo = await this.getAllPortInfo()

    const stats = {}

    for (const [id, pi] of allPortInfo) {
      const durations = await this.getPortCallDurations(pi, considerOmitted)
      const pcd = {
        port: pi.port,
        portCallDurations: Object.fromEntries(
          percentiles.map(percentile => {
            return [percentile, quantile(durations, percentile / 100)]
          })
        )
      }
      stats[id] = pcd
    }

    return stats
  }

  protected portCallDuration (pc: PortCall, considerOmitted: boolean): number {
    if (!considerOmitted && pc.isOmitted) {
      return 0
    }
    const arrival = new Date(pc.arrival)
    const departure = new Date(pc.departure)

    const durationMs = departure.getTime() - arrival.getTime()
    const durationHours = durationMs / 1000 / 3600
    return durationHours
  }

  protected async getPortCallDurations (
    portInfo: Readonly<PortInfo>,
    considerOmitted: boolean
  ): Promise<number[]> {
    return portInfo.portCalls.map(pc =>
      this.portCallDuration(pc, considerOmitted)
    )
  }
}

export default new PortService()
