import fastify from 'fastify'
import { join, dirname } from 'path'
// import fastifyNextJS from 'fastify-nextjs'
import HMR from 'fastify-webpack-hmr'
import { getVessels, getVesselInfo } from './api'
import ports from './portService'
import vessels from './vesselService'

const server = fastify()

const DEFAULT_LIMIT = 5
const DEFAULT_START = 0
const DEFAULT_PERCENTILES_PORTS = '5,20,50,75,90'
const DEFAULT_PERCENTILES_VESSELS = '5,50,80'
const DEFAULT_DAYS_VESSELS = '14,7,2'

const clientEntrypoint = (req, response) => {
  response
    .type('text/html; charset=utf-8')
    .send(`
<!DOCTYPE html>
<html lang='en'>
<head>
<meta charset='UTF-8'/>
<title>Portchain Frontend Demo</title>
<meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
<!-- CLIENT SCRIPT ROOT PLACEHOLDER -->
<div id='root'></div>
<!-- LOADS WEBPACK COMPILED ASSET [publicPath]/[filename] -->
<script src='/assets/client.js'></script>
</body>
</html>
  `)
}

server
  .register(HMR, {
    config: {
      mode: 'development',
      entry: [
        join(dirname(__dirname), 'client', 'index.tsx')
        // , 'webpack-hot-middleware/client'
      ],
      output: { filename: 'client.js', publicPath: '/assets' },
      resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
      },
      module: {
        rules: [
          {
            test: /\.tsx?$/,
            exclude: /node_modules/,
            use: ['babel-loader']
          },
          {
            test: /\.css/i,
            use: ['style-loader', 'css-loader']
          }
        ]
      }
    },
    webpackDev: {
      hot: false
    },
    webpackHot: false
  })
  .after(() => {
    // server.next('/')
    server.get('/', clientEntrypoint)
    server.get('/c/*', clientEntrypoint)

    server.get('/vessels', async (req, response) => {
      response.header('Content-Type', 'application/json')
      return JSON.stringify(await getVessels())
    })

    server.get('/vessel/:imo', async (req, response) => {
      const imo = (req.params as any).imo
      response.header('Content-Type', 'application/json')
      return JSON.stringify(await getVesselInfo(imo))
    })

    server.get('/top/ports', async (req, response) => {
      const query = req.query as any // Allow dot-notation
      const limit = parseInt(query.limit) || DEFAULT_LIMIT
      const start = parseInt(query.start) || DEFAULT_START
      const order = query.order
      if (order !== 'asc' && order !== 'desc') {
        response.status(400)
        throw new Error(
          'Parameter "order" not understood, valid values: [asc,desc]'
        )
      } else if (limit < 1) {
        response.status(400)
        return 'Bad request. Limit must be greater than 1.'
      }

      response.header('Content-Type', 'application/json')
      return await ports.getTopPorts({
        limit,
        start,
        order,
        // eslint-disable-next-line dot-notation
        considerOmitted: req.query['considerOmitted'] === 'true'
      })
    })

    server.get('/stats/ports', async (req, response) => {
      const percentilesStr: string =
        (req.query as any).percentiles || DEFAULT_PERCENTILES_PORTS
      const percentiles = percentilesStr.split(',').map(n => parseFloat(n))

      response.header('Content-Type', 'application/json')
      return await ports.getPortCallDurationPercentiles({
        percentiles,
        // eslint-disable-next-line dot-notation
        considerOmitted: req.query['considerOmitted'] === 'true'
      })
    })

    server.get('/stats/vessels', async (req, response) => {
      const percentilesStr: string = (req.query as any).percentiles || DEFAULT_PERCENTILES_VESSELS
      const daysStr: string = (req.query as any).days || DEFAULT_DAYS_VESSELS

      const percentiles = percentilesStr.split(',').map(n => parseFloat(n))
      const days = daysStr.split(',').map(n => parseInt(n))

      response.header('Content-Type', 'application/json')
      return await vessels.getPortCallDelayPercentiles({
        percentiles,
        days,
        // eslint-disable-next-line dot-notation
        considerOmitted: req.query['considerOmitted'] === 'true'
      })
    })

    server.get('/ping', async (req, response) => {
      return 'pong\n'
    })
  })

const serverOptions: { port: number; host?: string } = { port: 8080 }
if ('LISTEN_ADDRESS' in process.env && process.env.LISTEN_ADDRESS) {
  serverOptions.host = process.env.LISTEN_ADDRESS
}

server.listen(serverOptions, (err, address) => {
  if (err) {
    console.error(err)
    process.exit(1)
  }
  console.log(`Server listening at ${address}`)
})
