import { promises as fs } from 'fs'
import bent from 'bent'

import { Vessel, VesselInfo } from './api_types'

const getJSON = bent('json')

async function cachedRequest (imo: number | null = null): Promise<object> {
  let filename: string
  if (imo === null) {
    filename = 'cache/vessels'
  } else {
    filename = `cache/vessel-${imo}`
  }
  try {
    await fs.stat(filename)
  } catch (e) {
    let result: object
    if (e.code === 'ENOENT') {
      if (imo === null) {
        result = await getJSON(
          'https://import-coding-challenge-api.portchain.com/api/v2/vessels'
        )
      } else {
        result = await getJSON(
          `https://import-coding-challenge-api.portchain.com/api/v2/schedule/${imo}`
        )
      }
    } else {
      throw e
    }
    await fs.writeFile(filename, JSON.stringify(result))
  }
  const content = await fs.readFile(filename)
  return JSON.parse(content.toString('utf-8'))
}

export async function getVessels (): Promise<Vessel[]> {
  return (await cachedRequest()) as Vessel[]
}

export async function getVesselInfo (imo: number): Promise<VesselInfo> {
  return (await cachedRequest(imo)) as VesselInfo
}

export async function getAllVesselInfo (): Promise<VesselInfo[]> {
  const vessels = await getVessels()
  return await Promise.all(
    vessels.map(async vessel => {
      return await getVesselInfo(vessel.imo)
    })
  )
}
