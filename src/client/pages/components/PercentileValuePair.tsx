export const PercentileValuePair = ({ percentile, value }) => {
  return (
    <li>
      {percentile}%: {value.toPrecision(8)} hours
    </li>
  )
}
