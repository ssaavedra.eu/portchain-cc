import React from 'react'

export function InputBoxesWithButtons ({
  arr,
  onChangeItem,
  insertAt,
  deleteAt
}: {
  arr: (string | number | Readonly<string[]>)[]
  onChangeItem: (idx: number, e: React.ChangeEvent<HTMLInputElement>) => void
  insertAt: (
    idx: number,
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void
  deleteAt: (
    idx: number,
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void
}) {
  return (
    <>
      {arr.map((item, idx) => (
        <React.Fragment key={idx}>
          <input
            type='text'
            value={item}
            onChange={e => onChangeItem(idx, e)}
          />
          <button onClick={e => insertAt(idx, e)}>+</button>
          <button onClick={e => deleteAt(idx, e)}>-</button>
        </React.Fragment>
      ))}
    </>
  )
}
