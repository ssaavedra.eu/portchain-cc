import { ReloadType } from '../../utils'

export const ToggleOmittedStops = ({
  setConsiderOmitted,
  considerOmitted,
  reload
}: {
  setConsiderOmitted: (t: boolean) => void
  considerOmitted: boolean
  reload: ReloadType
}) => (
  <button
    onClick={e =>
      reload(setConsiderOmitted, considerOmitted, !considerOmitted, e)
    }
  >
    Toggle consider omitted stops (currently
    {considerOmitted ? ' considering them' : ' not considering them'})
  </button>
)
