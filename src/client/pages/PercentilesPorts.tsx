import React, { useEffect, useState } from 'react'
import { PortCallDurationPercentilesResult } from '../../server/api_types'
import { InputBoxesWithButtons } from './components/InputBoxesWithButtons'
import { PercentileValuePair } from './components/PercentileValuePair'
import { ToggleOmittedStops } from './components/ToggleOmittedStops'
import {
  reload as _reload,
  loadData,
  modifyArrayAndReload,
  reloadPercentile
} from '../utils'

const PercentilesPortsResults = ({
  content
}: {
  content: PortCallDurationPercentilesResult
}) => {
  return (
    <ul>
      {Object.values(content).map((value, idx) => {
        return (
          <li key={idx}>
            {value.port.name} ({value.port.id})
            <ul>
              {Object.entries(value.portCallDurations).map(
                ([percentile, value], idx) => (
                  <PercentileValuePair
                    key={idx}
                    percentile={percentile}
                    value={value}
                  />
                )
              )}
            </ul>
          </li>
        )
      })}
    </ul>
  )
}

export const PercentilesPorts = () => {
  const [percentiles, setPercentiles] = useState([5, 20, 50, 75, 90])
  const [isLoaded, setLoaded] = useState(false)
  const [considerOmitted, setConsiderOmitted] = useState(false)
  const [content, setContent] = useState(
    {} as PortCallDurationPercentilesResult
  )

  const reload = _reload(setLoaded)

  useEffect(
    loadData(
      '/stats/ports',
      {
        percentiles: percentiles.join(','),
        considerOmitted: considerOmitted ? 'true' : 'false'
      },
      isLoaded,
      setLoaded,
      setContent
    ),
    [percentiles, considerOmitted]
  )

  const addPercentile = modifyArrayAndReload(
    setPercentiles,
    percentiles,
    (idx, prev) => [
      ...prev.slice(0, idx + 1),
      prev[idx],
      ...prev.slice(idx + 1)
    ],
    reload
  )
  const removePercentile = modifyArrayAndReload(
    setPercentiles,
    percentiles,
    (idx, prev) => [...prev.slice(0, idx), ...prev.slice(idx + 1)],
    reload
  )

  return (
    <div>
      <h2>Percentiles for port call durations</h2>
      <nav className='top-ports-nav'>
        Percentiles:
        <ul className='percentiles'>
          <InputBoxesWithButtons
            arr={percentiles}
            onChangeItem={(idx, e) =>
              reloadPercentile(
                idx,
                e.target.value,
                percentiles,
                setPercentiles,
                setLoaded
              )
            }
            insertAt={addPercentile}
            deleteAt={removePercentile}
          />
        </ul>
        <ToggleOmittedStops
          setConsiderOmitted={setConsiderOmitted}
          considerOmitted={considerOmitted}
          reload={reload}
        />
      </nav>

      <PercentilesPortsResults content={content} />
    </div>
  )
}
