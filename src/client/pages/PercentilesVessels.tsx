import React, { useEffect, useState } from 'react'
import { PortCallDelayPercentileResult } from '../../server/api_types'
import { InputBoxesWithButtons } from './components/InputBoxesWithButtons'
import { PercentileValuePair } from './components/PercentileValuePair'
import { ToggleOmittedStops } from './components/ToggleOmittedStops'
import {
  loadData,
  modifyArrayAndReload,
  reload as _reload,
  reloadPercentile
} from '../utils'

const ListOfPercentilesPerDay = ({
  percentiles,
  day
}: {
  day: string
  percentiles: {
    [percentile: string]: number
  }
}) => {
  return (
    <li>
      {day}-day:
      <ul>
        {Object.entries(percentiles).map(([percentile, value], idx) => {
          return (
            <PercentileValuePair
              percentile={percentile}
              value={value}
              key={idx}
            />
          )
        })}
      </ul>
    </li>
  )
}

const PortCallDelaysPerDay = ({
  portCallDelays
}: {
  portCallDelays: {
    [day: string]: {
      [percentile: string]: number
    }
  }
}) => {
  return (
    <ul>
      {Object.entries(portCallDelays).map(([day, value], idx) => (
        <ListOfPercentilesPerDay key={idx} percentiles={value} day={day} />
      ))}
    </ul>
  )
}

const VesselsResult = ({
  content
}: {
  content: PortCallDelayPercentileResult
}) => {
  return (
    <ul>
      {Object.values(content).map((value, idx) => {
        return (
          <li key={idx}>
            {value.vessel.name} ({value.vessel.imo})
            <PortCallDelaysPerDay portCallDelays={value.portCallDelays} />
          </li>
        )
      })}
    </ul>
  )
}

export const PercentilesVessels = () => {
  const [percentiles, setPercentiles] = useState([5, 50, 80])
  const [days, setDays] = useState([14, 7, 2])
  const [isLoaded, setLoaded] = useState(false)
  const [considerOmitted, setConsiderOmitted] = useState(false)
  const [content, setContent] = useState({} as PortCallDelayPercentileResult)

  const reload = _reload(setLoaded)

  useEffect(
    loadData(
      '/stats/vessels',
      {
        percentiles: percentiles.join(','),
        days: days.join(','),
        considerOmitted: considerOmitted ? 'true' : 'false'
      },
      isLoaded,
      setLoaded,
      setContent
    ),
    [percentiles, days, considerOmitted]
  )

  const addPercentile = modifyArrayAndReload(
    setPercentiles,
    percentiles,
    (idx, prev) => [
      ...prev.slice(0, idx + 1),
      prev[idx],
      ...prev.slice(idx + 1)
    ],
    reload
  )
  const removePercentile = modifyArrayAndReload(
    setPercentiles,
    percentiles,
    (idx, prev) => [...prev.slice(0, idx), ...prev.slice(idx + 1)],
    reload
  )

  const addDay = modifyArrayAndReload(
    setDays,
    days,
    (idx, prev) => [
      ...prev.slice(0, idx + 1),
      prev[idx],
      ...prev.slice(idx + 1)
    ],
    reload
  )
  const removeDay = modifyArrayAndReload(
    setDays,
    days,
    (idx, prev) => [...prev.slice(0, idx), ...prev.slice(idx + 1)],
    reload
  )

  return (
    <div>
      <h2>Percentiles for vessel delays</h2>
      <nav className='top-ports-nav'>
        Percentiles:
        <ul className='percentiles'>
          <InputBoxesWithButtons
            arr={percentiles}
            onChangeItem={(idx, e) =>
              reloadPercentile(
                idx,
                e.target.value,
                percentiles,
                setPercentiles,
                setLoaded
              )
            }
            insertAt={addPercentile}
            deleteAt={removePercentile}
          />
        </ul>
        Days to consider:
        <ul className='days percentiles'>
          <InputBoxesWithButtons
            arr={days}
            onChangeItem={(idx, e) =>
              reloadPercentile(idx, e.target.value, days, setDays, setLoaded)
            }
            insertAt={addDay}
            deleteAt={removeDay}
          />
        </ul>
        <ToggleOmittedStops
          reload={reload}
          setConsiderOmitted={setConsiderOmitted}
          considerOmitted={considerOmitted}
        />
      </nav>

      <VesselsResult content={content} />
    </div>
  )
}
