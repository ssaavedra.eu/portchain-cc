import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useLocation
} from 'react-router-dom'

import TopPorts from './TopPorts'
import { PercentilesPorts } from './PercentilesPorts'
import { PercentilesVessels } from './PercentilesVessels'

import '../styles/globals.css'

export const Index = () => (
  <div>
    <h2>Welcome to the Portchain Coding Challenge Frontend site</h2>
    <p>
      Please use the links at the top in order to navigate through the web app.
    </p>
    <p>You can also, instead, use the API directly, powered by fastify.</p>
  </div>
)

export const NoMatch = () => (
  <h1>
    Error: route <code>{useLocation().pathname}</code> does not exist
  </h1>
)

const App = () => (
  <Router>
    <header className="top"><h1>Portchain Coding Challenge v17</h1></header>
    <div>
      <nav className='top'>
        <ul>
          <li>
            <Link to='/'>Home</Link>
          </li>
          <li>
            <Link to='/c/top-ports'>Top Ports</Link>
          </li>
          <li>
            <Link to='/c/percentiles-ports'>Port duration percentiles</Link>
          </li>
          <li>
            <Link to='/c/percentiles-vessels'>Vessel delay percentiles</Link>
          </li>
        </ul>
      </nav>
      <div id='main'>
        <Switch>
          <Route exact path='/'>
            <Index />
          </Route>
          <Route path='/c/top-ports'>
            <TopPorts />
          </Route>
          <Route path='/c/percentiles-ports'>
            <PercentilesPorts />
          </Route>
          <Route path='/c/percentiles-vessels'>
            <PercentilesVessels />
          </Route>
          <Route path=''>
            <NoMatch />
          </Route>
        </Switch>
      </div>
    </div>
    <footer>Made with ❤️ by <a href="mailto:info@ssaavedra.eu">Santiago Saavedra</a></footer>
  </Router>
)

export default App
