import React, { useEffect, useState } from 'react'
import { ToggleOmittedStops } from './components/ToggleOmittedStops'
import { loadData, reload as _reload } from '../utils'

const TopPorts = () => {
  const [order, setOrder] = useState('asc')
  const [num, setNum] = useState(5)
  const [considerOmitted, setConsiderOmitted] = useState(false)
  const [isLoaded, setLoaded] = useState(false)
  const [content, setContent] = useState({
    morePortsTied: false,
    total: 0,
    results: []
  })

  const reload = _reload(setLoaded)

  useEffect(
    loadData(
      '/top/ports',
      {
        order,
        limit: num.toString(),
        start: '0',
        considerOmitted: considerOmitted ? 'true' : 'false'
      },
      isLoaded,
      setLoaded,
      setContent
    ),
    [order, num, considerOmitted]
  )

  const renderElements = () => {
    return content.results.map((elt, idx) => {
      return (
        <li key={idx}>
          {elt.portId}: {elt.totalPortCalls} total port calls
        </li>
      )
    })
  }

  const maybeWarning = () => {
    if (content.morePortsTied) {
      return (
        <div className='warning'>
          More ports with the same number of calls are known but not shown due
          to size restrictions. Please increase the size to show them all.
        </div>
      )
    }
  }

  function onAddNum<T, E> (e: React.MouseEvent<T, E>) {
    reload(setNum, num, num + 1, e)
  }

  function onSubNum<T, E> (e: React.MouseEvent<T, E>) {
    reload(setNum, num, num - 1, e)
  }

  function onChangedElts (e: React.ChangeEvent<HTMLInputElement>) {
    const v = parseInt(e.target.value)
    if (!isNaN(v) && v > 0) {
      reload(setNum, num, v)
    } else {
      reload(setNum, num, 0)
    }
  }

  function onSetOrderAsc<T, E> (e: React.MouseEvent<T, E>) {
    reload(setOrder, order, 'asc', e)
  }

  function onSetOrderDesc<T, E> (e: React.MouseEvent<T, E>) {
    reload(setOrder, order, 'desc', e)
  }

  return (
    <div>
      <h2>Top Ports ({order})</h2>

      <nav className='top-ports-nav'>
        Sort:
        <ul>
          <li>
            <a
              href='#'
              onClick={onSetOrderAsc}
              className={order === 'asc' ? 'active' : ''}
            >
              ascending (fewest arrivals first)
            </a>
          </li>
          {' | '}
          <li>
            <a
              href='#'
              onClick={onSetOrderDesc}
              className={order === 'desc' ? 'active' : ''}
            >
              descending (most arrivals first)
            </a>
          </li>
        </ul>
        <br />
        <label>Number of elements to show</label>
        <input
          type='text'
          name='elts'
          size={4}
          onChange={onChangedElts}
          value={num}
        />
        <button onClick={onAddNum}>+</button>
        <button onClick={onSubNum}>-</button>
        <br />
        <ToggleOmittedStops
          setConsiderOmitted={setConsiderOmitted}
          considerOmitted={considerOmitted}
          reload={reload}
        />
      </nav>
      <ul>{renderElements()}</ul>
      {maybeWarning()}
    </div>
  )
}

export default TopPorts
