import React from 'react'
import ReactDOM from 'react-dom'
import App from './pages/index'

window.React = React

ReactDOM.render(<App />, document.getElementById('root'))
