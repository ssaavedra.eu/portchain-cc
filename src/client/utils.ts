import React from 'react'

/**
 * This function returns a function of (setter, cur, next, e?) that will see the setter to
 * pass next when cur != next, and do e.preventDefault if needed. It will also inform the
 * caller (via setLoaded) of the change
 * @param setLoaded This will receive false when the elements by the returned function have changed
 */
export function reload (
  setLoaded: React.Dispatch<React.SetStateAction<boolean>>
): ReloadType {
  return function <T, Elt, UIEvt> (
    setter: React.Dispatch<React.SetStateAction<T>>,
    cur: T,
    next: T,
    e?: React.UIEvent<Elt, UIEvt>
  ): void {
    if (e) {
      e.preventDefault()
    }
    if (next !== cur) {
      setter(next)
      setLoaded(false)
    }
  }
}

export type ReloadType = <T, Elt, UIEvt>(
  setter: React.Dispatch<React.SetStateAction<T>>,
  cur: T,
  next: T,
  e?: React.UIEvent<Elt, UIEvt>
) => void

export function loadData<T> (
  baseUrl: string,
  params: Record<string, string>,
  isLoaded: boolean,
  setLoaded: (boolean) => void,
  setContent: (t: T) => void
) {
  return () => {
    if (!isLoaded) {
      const url = new URL(`${window.location.origin}${baseUrl}`)
      Object.entries(params).forEach(([k, v]) => url.searchParams.append(k, v))

      fetch(url.toString(), {
        method: 'GET'
      })
        .then(response => response.json())
        .then(json => {
          setContent(json)
          setLoaded(true)
        })
    }
  }
}

export function reloadPercentile (
  idx: number,
  percentile: string,
  percentiles: number[],
  setPercentiles: (t: number[]) => void,
  setLoaded: (t: boolean) => void
) {
  const percentileFloat = parseFloat(percentile)
  if (
    percentileFloat >= 0 &&
    percentileFloat <= 100 &&
    !isNaN(percentileFloat) &&
    percentiles[idx] !== percentileFloat
  ) {
    const newPercentiles = [...percentiles]
    newPercentiles[idx] = percentileFloat
    reload(setLoaded)(setPercentiles, percentiles, newPercentiles)
  }
}

export function modifyArrayAndReload<V, T, E> (
  setArray: (v: V[]) => void,
  prev: V[],
  modify: (idx: number, v: V[]) => V[],
  reload: ReloadType
) {
  return (idx: number, e: React.MouseEvent<T, E>) => {
    reload(setArray, prev, modify(idx, prev), e)
  }
}
