import assert from 'assert'
import { PortCall } from '../src/server/api_types'
import { VesselService } from '../src/server/vesselService'

const pdfTestCase: PortCall = {
  arrival: '2019-03-15T13:00:00+00:00',
  departure: '',
  createdDate: '',
  isOmitted: false,
  service: 'Test Service',
  port: {
    id: 'DJPOD',
    name: 'Djibouti'
  },
  logEntries: [
    {
      updatedField: 'arrival',
      arrival: '2019-03-15T18:00:00+00:00',
      createdDate: '2019-03-01T00:00:00+00:00'
    },
    {
      updatedField: 'arrival',
      arrival: '2019-03-15T10:00:00+00:00',
      createdDate: '2019-03-13T12:59:00+00:00'
    },
    {
      updatedField: 'arrival',
      arrival: '2019-03-15T13:00:00+00:00',
      createdDate: '2019-03-15T13:00:00+00:00'
    }
  ]
}

describe('VesselService', function () {
  describe('#getPortCallDelay()', function () {
    it('should return 3 hours for the testcase in 2-day delay', function () {
      class VesselServiceTest extends VesselService {
        test () {
          assert.strictEqual(
            this.getPortCallDelay(pdfTestCase, 2),
            3,
            'Expected and actual arrival delay times do not match'
          )
        }
      }
      new VesselServiceTest().test()
    })

    it('should return 5 hours for the testcase in 7-day delay', function () {
      class VesselServiceTest extends VesselService {
        test () {
          assert.strictEqual(
            this.getPortCallDelay(pdfTestCase, 7),
            5,
            'Expected and actual arrival delay times do not match'
          )
        }
      }
      new VesselServiceTest().test()
    })

    it('should return 5 hours for the testcase in 14-day delay', function () {
      class VesselServiceTest extends VesselService {
        test () {
          assert.strictEqual(
            this.getPortCallDelay(pdfTestCase, 14),
            5,
            'Expected and actual arrival delay times do not match'
          )
        }
      }
      new VesselServiceTest().test()
    })
  })
})
