import assert from 'assert'
import { VesselInfo } from '../src/server/api_types'
import { PortService } from '../src/server/portService'

const testVesselInfo: VesselInfo[] = [
  {
    vessel: {
      imo: 1,
      name: 'Test Vessel 1'
    },
    portCalls: [
      {
        arrival: '2019-03-15T13:00:00+00:00',
        departure: '2019-03-15T14:00:00+00:00',
        createdDate: '',
        isOmitted: false,
        service: 'Test Service',
        port: {
          id: 'TEST1',
          name: 'Test Port 1'
        },
        logEntries: []
      },
      {
        arrival: '2019-03-15T13:40:00+00:00',
        departure: '2019-03-15T14:40:00+00:00',
        createdDate: '',
        isOmitted: false,
        service: 'Test Service',
        port: {
          id: 'TEST2',
          name: 'Test Port 2'
        },
        logEntries: []
      }
    ]
  },
  {
    vessel: {
      imo: 2,
      name: 'Test Vessel 2'
    },
    portCalls: [
      {
        arrival: '2019-03-15T13:10:00+00:00',
        departure: '2019-03-15T14:10:00+00:00',
        createdDate: '',
        isOmitted: false,
        service: 'Test Service',
        port: {
          id: 'TEST1',
          name: 'Test Port 1'
        },
        logEntries: []
      },
      {
        arrival: '2019-03-15T13:30:00+00:00',
        departure: '2019-03-15T14:30:00+00:00',
        createdDate: '',
        isOmitted: false,
        service: 'Test Service',
        port: {
          id: 'TEST2',
          name: 'Test Port 2'
        },
        logEntries: []
      }
    ]
  },
  {
    vessel: {
      imo: 3,
      name: 'Test Vessel 3'
    },
    portCalls: [
      {
        arrival: '2019-03-15T13:00:00+00:00',
        departure: '2019-03-15T14:00:00+00:00',
        createdDate: '',
        isOmitted: false,
        service: 'Test Service',
        port: {
          id: 'TEST1',
          name: 'Test Port 1'
        },
        logEntries: []
      },
      {
        arrival: '2019-03-15T14:00:00+00:00',
        departure: '2019-03-15T15:00:00+00:00',
        createdDate: '',
        isOmitted: false,
        service: 'Test Service',
        port: {
          id: 'TEST2',
          name: 'Test Port 2'
        },
        logEntries: []
      },
      {
        arrival: '2019-03-15T15:00:00+00:00',
        departure: '2019-03-15T16:00:00+00:00',
        createdDate: '',
        isOmitted: false,
        service: 'Test Service',
        port: {
          id: 'TEST3',
          name: 'Test Port 3'
        },
        logEntries: []
      }
    ]
  }
]

describe('PortService', function () {
  describe('#getTopPorts()', function () {
    it('should return tie=true when there is a tie', async function () {
      class PortServiceTest extends PortService {
        getAllVesselInfo = async function () {
          return testVesselInfo
        }

        async test () {
          const result = await this.getTopPorts({
            limit: 1,
            order: 'desc',
            start: 0
          })
          assert.strictEqual(result.morePortsTied, true)
          assert.strictEqual(result.total, 3)
        }
      }
      await new PortServiceTest().test()
    })

    it('should return tie=false when there is no tie', async function () {
      class PortServiceTest extends PortService {
        getAllVesselInfo = async function () {
          return testVesselInfo
        }

        async test () {
          assert.notStrictEqual(
            await this.getTopPorts({ limit: 2, order: 'desc', start: 0 }),
            {
              morePortsTied: false,
              results: [
                {
                  portId: 'TEST1',
                  totalPortCalls: 3
                },
                {
                  portId: 'TEST2',
                  totalPortCalls: 3
                }
              ],
              total: 3
            }
          )
        }
      }
      await new PortServiceTest().test()
    })

    it('should return at most as many elements as requested', async function () {
      class PortServiceTest extends PortService {
        getAllVesselInfo = async function () {
          return testVesselInfo
        }

        async test () {
          assert.ok(
            (await this.getTopPorts({ limit: 4, order: 'desc', start: 0 }))
              .results.length <= 4
          )
        }
      }
      await new PortServiceTest().test()
    })
  })

  it('should omit ports with omited port calls when no other ports calls are available', async function () {
    class PortServiceTest extends PortService {
      getAllVesselInfo = async () => {
        return [
          {
            vessel: {
              imo: 1,
              name: 'Test Vessel 1'
            },
            portCalls: [
              {
                arrival: '2019-03-15T13:00:00+00:00',
                departure: '2019-03-15T14:00:00+00:00',
                createdDate: '',
                isOmitted: true,
                service: 'Test Service',
                port: {
                  id: 'TEST1',
                  name: 'Test Port 1'
                },
                logEntries: []
              },
              {
                arrival: '2019-03-15T13:40:00+00:00',
                departure: '2019-03-15T14:40:00+00:00',
                createdDate: '',
                isOmitted: false,
                service: 'Test Service',
                port: {
                  id: 'TEST2',
                  name: 'Test Port 2'
                },
                logEntries: []
              },
              {
                arrival: '2019-03-15T13:40:00+00:00',
                departure: '2019-03-15T14:40:00+00:00',
                createdDate: '',
                isOmitted: false,
                service: 'Test Service',
                port: {
                  id: 'TEST2',
                  name: 'Test Port 2'
                },
                logEntries: []
              }
            ]
          }
        ]
      }

      async test () {
        const result = await this.getTopPorts({
          limit: 2,
          order: 'desc',
          considerOmitted: false
        })
        assert.strictEqual(result.results.length, 1)
        assert.strictEqual(result.results[0].portId, 'TEST2')
        assert.strictEqual(result.results[0].totalPortCalls, 2)
      }
    }
    await new PortServiceTest().test()
  })

  it('should not omit ports with omited port calls when no other ports calls are available if considerOmitted=false', async function () {
    class PortServiceTest extends PortService {
      getAllVesselInfo = async () => {
        return [
          {
            vessel: {
              imo: 1,
              name: 'Test Vessel 1'
            },
            portCalls: [
              {
                arrival: '2019-03-15T13:00:00+00:00',
                departure: '2019-03-15T14:00:00+00:00',
                createdDate: '',
                isOmitted: true,
                service: 'Test Service',
                port: {
                  id: 'TEST1',
                  name: 'Test Port 1'
                },
                logEntries: []
              },
              {
                arrival: '2019-03-15T13:40:00+00:00',
                departure: '2019-03-15T14:40:00+00:00',
                createdDate: '',
                isOmitted: false,
                service: 'Test Service',
                port: {
                  id: 'TEST2',
                  name: 'Test Port 2'
                },
                logEntries: []
              },
              {
                arrival: '2019-03-15T13:40:00+00:00',
                departure: '2019-03-15T14:40:00+00:00',
                createdDate: '',
                isOmitted: false,
                service: 'Test Service',
                port: {
                  id: 'TEST2',
                  name: 'Test Port 2'
                },
                logEntries: []
              }
            ]
          }
        ]
      }

      async test () {
        const result = await this.getTopPorts({
          limit: 2,
          order: 'desc',
          considerOmitted: true
        })
        assert.strictEqual(result.results.length, 2)
        assert.strictEqual(result.results[0].portId, 'TEST2')
        assert.strictEqual(result.results[0].totalPortCalls, 2)
        assert.strictEqual(result.results[1].portId, 'TEST1')
        assert.strictEqual(result.results[1].totalPortCalls, 1)
      }
    }
    await new PortServiceTest().test()
  })

  describe('#getPortCallDurationPercentiles', function () {
    it('should work for the simple scenario', async function () {
      class PortServiceTest extends PortService {
        getAllVesselInfo = async function () {
          return testVesselInfo
        }

        async test () {
          const result = await this.getPortCallDurationPercentiles({
            percentiles: [50]
          })
          // eslint-disable-next-line dot-notation
          assert.strictEqual(result['TEST1'].portCallDurations[50], 1)
        }
      }
      await new PortServiceTest().test()
    })
  })

  describe('#portCallDuration', function () {
    it('should omit the omitted arrivals', async function () {
      class PortServiceTest extends PortService {
        test () {
          assert.strictEqual(
            this.portCallDuration(
              {
                arrival: '2019-03-15T15:00:00+00:00',
                departure: '2019-03-15T16:00:00+00:00',
                createdDate: '2019-03-15T15:00:00+00:00',
                isOmitted: true,
                service: 'Test',
                logEntries: []
              },
              false
            ),
            0
          )
        }
      }
      new PortServiceTest().test()
    })
    it('should not omit the non-omitted arrivals', async function () {
      class PortServiceTest extends PortService {
        test () {
          assert.strictEqual(
            this.portCallDuration(
              {
                arrival: '2019-03-15T15:00:00+00:00',
                departure: '2019-03-15T16:00:00+00:00',
                createdDate: '2019-03-15T15:00:00+00:00',
                isOmitted: false,
                service: 'Test',
                logEntries: []
              },
              false
            ),
            1
          )
        }
      }
      new PortServiceTest().test()
    })
  })
})
