Portchain Statistics API Server
===============================

This statistics server covers the Coding Challenge v17 for Portchain.

This implementation is done by Santiago Saavedra. It is made in TypeScript and has mocha testing support. Thanks for looking at this code.

The canonical URL for this repository is: https://gitlab.com/ssaavedra.eu/portchain-cc but you may have received a zip-version as an attachment.

In order to minimize queries to the outside world, a writable cache/ folder is expected to be present in the file system. The cached information will never be reloaded, and the cache is not expected to be production-quality.

There is a web-based frontend built within the solution, but it can also be used as a standalone API.

The web server is not expected to be production-grade since that would require interacting with more tools than the assumptions that can be made to evaluate this code and, in the author's opinion, would not contribute much to the evaluation. Thus, no caching is performed on the client-side (at the frontend), since the data is expected to be "locally" available.

Running and hacking
===================

The author used [pnpm](https://pnpm.js.org/) as a package manager. You should be able to run this with npm if you prefer, but he recommends pnpm due to it being more efficient with space.

In order to run this exercise, you will require a Node.js-supported environment. You are expected to have [Node.js installed as well as npm](https://nodejs.org/en/).

If you don't have `pnpm` installed, you can install it globally with `npm install -g pnpm`. You might need to run this command with elevated privileges (e.g., `sudo`) depending on your environment.

In the folder where you have this file available with the rest of the code, please perform the following commands:

```bash
pnpm install
```

In order to run the API server, you can run:

```bash
pnpm run start:norestart
```

An additional `start:dev` command exists in case you want to hack into the code, using nodemon to restart the server when the code changes.

A `pnpm run test` will run mocha (you could do `npx mocha` instead too).


Interaction
===========

This is meant to run as a node.js application (with ts-node).

In order to run it, just do `pnpm run start:norestart` from a bash shell.

When it is running, you should see `Server listening at http://127.0.0.1:8080`. If that is the case, you will have the following endpoints:

- http://127.0.0.1:8080/ will lead to the React-based frontend interface, served by webpack
- http://127.0.0.1:8080/top/ports Will contain the Top-5 ports with the most arrivals, with the total number of port calls for each port. You can alter the following query parameters: `limit` (int), `start` (int) and `order` ("asc" or "desc").
- http://127.0.0.1:8080/top/ports?order=asc will contain the ports with the fewest port calls.
- http://127.0.0.1:8080/stats/ports will contain the ports by percentiles of their port call duration in hours. By default, it uses the percentiles specified in the requirements, but they can be changed with a query string like `?percentiles=1,2,3,50,99`
- http://127.0.0.1:8080/stats/vessels will contain the vessel percentiles by their delay in hours. By default, it uses the percentiles specified in the requirements, but can be changed with a query string like `?percentiles=1,2,50,99`.

All these endpoints can receive a `considerOmitted=true` to consider all events where `portCall.isOmitted=true`, where they would otherwise be omitted.


There are also `/vessels` and `/vessel/:imo` endpoints for debugging purposes.

Design choices
==============

When no ships have had a portCall or logEntries that change the arrival time, the delay is set to be zero, since it's assumed that the input time was correct from the beginning.

Top 5: some ports could have the same number of arrivals or port calls than others. However, we are asked for the top 5 and no other means for disambiguation for who to chose in case of ties.

Since there is no obvious tie-breaker, I decided to output a "morePortsTied" boolean field which indicates that the last element in the top 5 is in a tie with further elements. Since the API is paginated and allows for different limits, different visualizations could make use of that information.

Omitted port calls are considered to have duration=0. We are not sure if this is the right approach. This could be changed by setting considerOmitted=true on the API calls.

We do not consider changes in isOmitted when calculating the vessel delays (i.e., changes in isOmitted through the eventLog), because we consider that even if we are looking into the past, if isOmitted=true, the *actual* arrival date may be bogus and should not be taken into consideration.

About the API objects (getAllVessels et al.):

I could have used "active" objects when returning them from the API.

Instead, I chose to do the stats calcuations afterwards because that way I think it's easier to debug where can the bottlenecks be, for improved maintainability.
